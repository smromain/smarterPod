var hasBeenLoaded = false;

angular.module('smarterpodApp')
    .controller('playlistCtrl', function ($scope, $http, playlistCreation, $sce, playlistLoader, playerLoader, $q) {

        var deferUntilFeedRetrieved = $q.defer();

        chrome.storage.sync.get(null, function (items) {
            playlistCreation.userFeed = items.userFeed || [];
            deferUntilFeedRetrieved.resolve();
        });

        deferUntilFeedRetrieved.promise.then(function () {
            if (hasBeenLoaded === false) {
                this.loaded = playlistLoader.initialLoad();
                this.loaded.then(
                    function success(data) {
                        $scope.$emit('playlistLoaded')
                    },
                    function error(data) {
                        $scope.$emit('playlistLoaded')
                    }
                );
                hasBeenLoaded = true;
            }
        })


        $scope.playlistItems = playlistCreation.playlist || [];

//the podlist sync method will have to be refactored, as only 3-4 podcasts can be saved and synched to the cloud currently. the playlist functions still work, but we need to split the object into multiple objects, and then create a 'map' for reference

        $scope.addTrack = function (index) {
            // var deferral = $q.defer();
            if ($scope.podPlayer.currentTime > 0.1) {
                $scope.saveState($scope.podPlayer.currentTime)
            }
            playerLoader.loadPlayer($scope, index, false, 'add');
            var sanitizedPodList = [];

            $scope.podList.forEach(function (item) {
                sanitizedPodList.push(item)
            });

            $scope.podList.forEach(function (item, index) {
                sanitizedPodList[index].src = item.src.toString();
                sanitizedPodList[index].image = item.image.toString();
                sanitizedPodList[index].description = item.description.substring(0, 180);
                sanitizedPodList[index].added = Date.now();
            });

            chrome.storage.sync.set({playlist: sanitizedPodList}, function () {
                console.log('data is pushed');
                // deferral.resolve('42');
            });
            // return deferral.promise;
        };

        $scope.replaceTrack = function (index) {
            var deferral = $q.defer();
            playerLoader.loadPlayer($scope, index, true, 'replace');
            var sanitizedPodList = $scope.podList;
            sanitizedPodList.forEach(function (item) {
                item.description = item.description.substring(0, 180);
                item.src = item.src.toString();
                item.image = item.image.toString();
                item.added = Date.now();
            });

            chrome.storage.sync.set({playlist: sanitizedPodList}, function () {
                console.log('data is pushed');
                deferral.resolve('42');
            });
            return deferral.promise;
        };

        $scope.saveState = function (data) {
            var deferral = $q.defer();
            var locationInPlaylist = $scope.podPlayer.currentTrack - 1;
            var sanitizedPodList = $scope.podList;

            sanitizedPodList.forEach(function (item) {
                item.description = item.description.substring(0, 180);
                item.src = item.src.toString();
                item.image = item.image.toString();
                item.added = Date.now();
            });

            var itemToSanitize = sanitizedPodList[locationInPlaylist];
            itemToSanitize.listened = $scope.podPlayer.currentTime;

            chrome.storage.sync.set({playlist: sanitizedPodList}, function () {
                console.log('data is pushed');
                deferral.resolve('42');
            });

        };

        $scope.nullOutPlayer = function (index) {
            playerLoader.loadPlayer($scope, null, null, 'clear')
        };

        $scope.addPodcast = function (name, feed) {
            if (name === undefined || feed === undefined) {
                throw "Incorrect input!";
            }
            else {
                playlistCreation.userFeed.push({name: name, rss: feed});
                chrome.storage.sync.get(null, function (items) {
                    var synchedFeeds = items.userFeed || [];
                    var feedObj = {name: name, rss: feed};
                    synchedFeeds.push(feedObj);
                    chrome.storage.sync.set({userFeed: synchedFeeds});
                    $scope.$digest();
                });
                var localLocation = playlistCreation.userFeed.length - 1;
                var createdObject = playlistCreation.userFeed[localLocation];
                playlistLoader.addPodcast(createdObject.name, createdObject.rss);
            }
        };


        $scope.searchForPodcasts = function (name) {
            return playlistLoader.retrievePodcastInfoFromiTunes(name);
        };


        $scope.saveFeedlist = function () {
            chrome.storage.sync.get(null, function (items) {
                var unsynchedFeeds = playlistCreation.userFeed;
                chrome.storage.sync.set({userFeed: unsynchedFeeds});
                $scope.$digest();
            });
        };

//TO DO: add functionality to call X episodes of whatever feed

        $scope.olderEpisodeRetriever = function (name, feed, index) {
            playlistLoader.retrieveOlderEpisodes(name, feed, index);
            $scope.$digest();
        };

        $scope.resyncUpdateFeed = function () {
            playlistLoader.resyncUpdateFeed();
        }

    });

