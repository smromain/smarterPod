'use strict';

angular.module('smarterpodApp')
  .directive('playlist', function () {
    return {
      templateUrl: 'app/playlist/playlist.html',
      restrict: 'EA',
      controller: 'playlistCtrl',
      link: function (scope, element, attrs) {
      }
    };
  });