angular.module('smarterpodApp')
.controller('playerCtrl', function($scope, $http, playerLoader, playlistCreation, $timeout, $q, $interval, $sce) {

playerScope = $scope;

$scope.$on('playlistLoaded', 
	function() {
		//setup backup interval function for checking feeds once per hour for refresh, this will check to see if the popup is engaged (via playing or open popup)
		$interval(function () {
			var windowsOpen = chrome.extension.getViews();
		    if (playerScope.podPlayer.playing === false && windowsOpen.length < 2) {
		        chrome.runtime.reload();
		      }
		}, 3600000);
		playerLoader.loadPlayer(playerScope, 0, false, 'clear');
		$scope.podPlayer.on('ended', function (evt) {
			//advance track and reload player data
    		var newTrack = playerScope.podPlayer.currentTrack + 1;
    		var newLocationMap = playerScope.podPlayer.currentTrack;
    		$timeout(function() {
    			$scope.playNow(playerScope.podList[newLocationMap].location, newTrack);
    		}, 1500);
  		});
  		chrome.storage.sync.get(null, function (items) {
  			if (items.playlist !== undefined) {
  				playerScope.podList = playerScope.podList.concat(items.playlist);
  			}
        });
	}
);

$scope.playNow = function(location, index, optionalArgument, /*not working*/ progress ) {
	var staggerPlayMethods = $q.defer();
	console.log(location, index, optionalArgument)
	console.log(playerScope)

	if (optionalArgument === 'saved') {
		staggerPlayMethods.resolve(playerLoader.loadPlayer(playerScope, index, false, 'savedSwitch'));
	}
	else {
		staggerPlayMethods.resolve(playerLoader.loadPlayer(playerScope, location, false, 'switch'));
	}

	staggerPlayMethods.promise.then(function(){
		playerScope.podPlayer.play(index)
	})

	.then($scope.podPlayer.one('play', function(evt){

		if (optionalArgument == 'saved') {
			staggerPlayMethods.promise.then(
				function() {
						var filteredItem = playerScope.podList.filter(function(elem){
							return elem.location === location;
						})
						var listenedToPoint = filteredItem[0].listened || 0;

						$scope.podPlayer.seek(listenedToPoint);
				}
			)
		}


		staggerPlayMethods.promise.then($timeout(function() {staggerPlayMethods.promise.then(
			function(){
				var options = {type: 'basic', title: 'Now Playing in SmarterPod:', message: $scope.podPlayer.podcastName + ' : ' + $scope.podPlayer.episode, priority: 2}
				var xhr = new XMLHttpRequest();
				xhr.open("GET", $sce.trustAsResourceUrl($scope.podPlayer.image));
				xhr.responseType = "blob";
				xhr.onload = function(){
			        var blob = this.response;
			        options.iconUrl = window.URL.createObjectURL(blob);
			        chrome.notifications.create('id1', options, function() {});
			    };
				xhr.send(null)
			}

			)}, 250));
	}))

};

$scope.removeItemFromPodList = function(index, nextItemData) {
	
	var podListStagger = $q.defer();

	podListStagger.resolve($scope.podList.splice(index, 1));

	podListStagger.promise.then(function() {
		chrome.storage.sync.set({playlist: $scope.podList});
	});

};


$scope.playlistInteraction = playlistCreation;

});