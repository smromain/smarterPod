'use strict';

angular.module('smarterpodApp')
  .directive('playermodule', function () {
    return {
      templateUrl: 'app/playermodule/playermodule.html',
      restrict: 'EA',
      controller: 'playerCtrl as player',
      link: function (scope, element, attrs) {
      }
    };
  });