'use strict';

describe('Directive: playermodule', function () {

  // load the directive's module and view
  beforeEach(module('smarterpodApp'));
  beforeEach(module('app/playermodule/playermodule.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<playermodule></playermodule>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the playermodule directive');
  }));
});