'use strict';

angular.module('smarterpodApp')
    .config(function ($httpProvider) {
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    })
    .factory('playlistLoader', function ($sce, $http, playlistCreation, $timeout, $q, $rootScope) {
        function getEpisodeAndAddToPlaylist(podcastName, data, index) {
            if (data === null) {
                Bugsnag.notify('RSS feed could not be found: ', JSON.stringify(data));
                $rootScope.messageService = "Podcast not found!";
                throw "No podcast could be found!";
            }
            var channel = function () {
                if (data.rss.channel) {
                    return data.rss.channel
                }
                else {
                    console.log(data);
                    Bugsnag.notify('Error on following feed', data)
                }
            }();
            var episodeTitle = function () {
                if (channel.item[index]) {
                    return channel.item[index].title;
                }
                else {
                    return channel.item.title;
                }
                ;
            }();
            var episodeDesc = function () {
                var tmp = document.createElement('DIV');
                if (channel.item[index]) {
                    tmp.innerHTML = channel.item[index].description;
                    return tmp.innerText;
                }
                else {
                    return channel.item.description;
                }
                ;
            }();
            var episodeUrl = function () {
                if (channel.item[index]) {
                    if (channel.item[index].enclosure.length > 1) {
                        return channel.item[index].enclosure[0]._url;
                    }
                    else {
                        return channel.item[index].enclosure._url;
                    }
                }
                else {
                    if (channel.item.enclosure.length > 1) {
                        return channel.item.enclosure[0]._url;
                    }
                    else {
                        return channel.item.enclosure._url;
                    }
                }
                ;
            }();
            var imageUrl = function () {
                if (channel.image[0]) {
                    if (channel.image[0].hasOwnProperty('_href')) {
                        return channel.image[0]._href;
                    }
                    else if (channel.image[0].hasOwnProperty('url')) {
                        return channel.image[0].url;
                    }
                }
                else {
                    if (channel.image.hasOwnProperty('_href')) {
                        return channel.image._href;
                    }
                    else if (channel.image.hasOwnProperty('url')) {
                        return channel.image.url;
                    }
                }
                ;
            }();

            playlistCreation.playlist.push({
                podcast: podcastName,
                episode: episodeTitle,
                description: episodeDesc,
                added: Date.now(),
                location: playlistCreation.playlist.length,
                image: function () {
                    return $sce.trustAsResourceUrl(imageUrl);
                }(),
                url: function () {
                    return $sce.trustAsResourceUrl(episodeUrl);
                }()
            })
        };
        //resolve a promise afer this is finished
        function playlistInitializer() {
            var rssSaves = [];
            if (playlistCreation.userFeed === undefined) {
                return rssFeeds
            }
            playlistCreation.userFeed.forEach(function (item) {
                rssSaves.push(
                    // $http.get('http://query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent('select * from xml where url=\"') + encodeURIComponent(item.rss) + '\"&format=json&diagnostics=true&callback=')
                    $http.get(item.rss)
                        .then(function (resp) {
                            getEpisodeAndAddToPlaylist(item.name, resp.data, 0);
                            return item.name;
                        }, function (resp) {
                            console.log(item.name + ' could not be loaded.')
                            return Bugsnag.notify('Podcast could not be loaded: ', JSON.stringify(item));
                        })
                );
            });
            var rssFeeds = $q.all(rssSaves);
            return rssFeeds;
        }

        function episodeMetadataRetriever(itemName, itemFeed, index) {
            if (!index) var index = 0;
            $http.get(itemFeed).success(function (data) {
                getEpisodeAndAddToPlaylist(itemName, data, index)
            })
        }

        function retrievePodcastInfoFromiTunes(itemName) {
            var itemNameEncoded = encodeURIComponent(itemName);
            return $http.get('https://itunes.apple.com/search?media=podcast&term=' + itemNameEncoded)
                .then(function (response) {
                    var data = response.data || [];
                    if (data.resultCount === 0){
                        return [{failureMessage:"No podcasts could be found with the search term '" + itemName + "'."}];
                    }
                    var modifiedData = data.results.map(function (item) {
                        return {name: item.collectionName, artist: item.artistName, image: item.artworkUrl100, rss: item.feedUrl}
                    });
                    return modifiedData;
                }, function (err) {
                    return [{failureMessage:"No podcasts could be found with the search term '" + itemName + "'."}]
                });
        }

        var initialized = false;

        // Public API here
        return {
            initialLoad: function () {
                if (initialized === false) {
                    var promiseForPlaylistReady = playlistInitializer();
                    return promiseForPlaylistReady;
                }
                return false;
            },
            addPodcast: function (itemName, itemFeed) {
                if (itemName === undefined || itemFeed === undefined) {
                    throw new Error('Incorrect name or feed!')
                }
                return episodeMetadataRetriever(itemName, itemFeed);
            },
            retrievePodcastInfoFromiTunes: function (itemName) {
                return retrievePodcastInfoFromiTunes(itemName)
            },
            retrieveOlderEpisodes: function (itemName, itemFeed, index) {
                return episodeMetadataRetriever(itemName, itemFeed, index);
            },
            resyncUpdateFeed: function () {
                var promiseForPlaylistReady = playlistInitializer();
                return promiseForPlaylistReady;
            }
        };
    });
