'use strict';

describe('Service: playlistLoader', function () {

  // load the service's module
  beforeEach(module('smarterpodApp'));

  // instantiate service
  var playlistLoader;
  beforeEach(inject(function (_playlistLoader_) {
    playlistLoader = _playlistLoader_;
  }));

  it('should do something', function () {
    expect(!!playlistLoader).toBe(true);
  });

});
