'use strict';

angular.module('smarterpodApp')
  .factory('playerLoader', function (playlistCreation, $q) {
    var resolvePlayer = function (scope,index,addClearReplaceOrSwitch) {
        switch (addClearReplaceOrSwitch) {
          case 'replace':
              scope.podList = [];
              scope.podPlayer.podcastName = scope.playlistArray[index].podcast;
              scope.podPlayer.image = scope.playlistArray[index].image;
              scope.podPlayer.episode = scope.playlistArray[index].episode;
              scope.podPlayer.description = scope.playlistArray[index].description;
              scope.podPlayer.location = scope.playlistArray[index].location;
              scope.podPlayer.urlStore = scope.playlistArray[index].url;
              scope.podList.push({src: scope.playlistArray[index].url, episode: scope.playlistArray[index].episode, name: scope.playlistArray[index].podcast, description: scope.playlistArray[index].description, image: scope.playlistArray[index].image, location: scope.playlistArray[index].location, added: Date.now()});
              break

        case 'switch': 
              scope.podPlayer.podcastName = scope.playlistArray[index].podcast;
              scope.podPlayer.image = scope.playlistArray[index].image;
              scope.podPlayer.episode = scope.playlistArray[index].episode;
              scope.podPlayer.description = scope.playlistArray[index].description;
              scope.podPlayer.location = scope.playlistArray[index].location;
              scope.podPlayer.urlStore = scope.playlistArray[index].url;
              break;

        case 'savedSwitch': 
              scope.podPlayer.podcastName = scope.podList[index].name;
              scope.podPlayer.image = scope.podList[index].image;
              scope.podPlayer.episode = scope.podList[index].episode;
              scope.podPlayer.description = scope.podList[index].description;
              scope.podPlayer.location = scope.podList[index].location;
              scope.podPlayer.urlStore = scope.podList[index].url;
              break;
        
        case 'add': 
              scope.podList.push({src: scope.playlistArray[index].url, episode: scope.playlistArray[index].episode, name: scope.playlistArray[index].podcast, description: scope.playlistArray[index].description, image: scope.playlistArray[index].image, location: scope.playlistArray[index].location, added: Date.now()});
          break;

        case 'clear': 
              scope.podPlayer.podcastName = null;
              scope.podPlayer.image = '';
              scope.podPlayer.episode = null;
              scope.podPlayer.description = null;
              scope.podPlayer.location = null;
          break;
        }
    }

    // Public API here
    return {
      loadPlayer: function(scope,index,autoplay,addClearReplaceOrSwitch){
        scope.playlistArray = playlistCreation.playlist;
          resolvePlayer(scope,index,addClearReplaceOrSwitch);
        }
      }
  });
