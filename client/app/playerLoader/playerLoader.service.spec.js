'use strict';

describe('Service: playerLoader', function () {

  // load the service's module
  beforeEach(module('smarterpodApp'));

  // instantiate service
  var playerLoader;
  beforeEach(inject(function (_playerLoader_) {
    playerLoader = _playerLoader_;
  }));

  it('should do something', function () {
    expect(!!playerLoader).toBe(true);
  });

});
