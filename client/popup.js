var currentPage = chrome.extension.getBackgroundPage();

angular.module('smarterPodRemote', ['ngCookies',
    'ngResource',
    'ngSanitize',
    'ngDialog']).config(function ($provide) {
        $provide.decorator("$exceptionHandler", ['$delegate', function ($delegate) {
            return function (exception, cause) {
                Bugsnag.notify(exception);
                $delegate(exception, cause);
            }
        }])
    })
    .controller('remote', function ($scope, $interval, $timeout, $q, ngDialog, $rootScope) {

        $scope.smarterPod = currentPage.playerScope;
        $scope.currentPlayingImg = $scope.smarterPod.podPlayer.image.toString();
        $scope.truncatedDesc = 500;
        $scope.playlistLength = null;
        $scope.fastForwardLength = 60;

        $interval(function () {
            $scope.playlistLength = $scope.smarterPod.podList.length
        }, 1000);

        //refresh background page every 1 hour when the page is not playing a file, to check for feed updates
        $interval(function () {
            if ($scope.smarterPod.podPlayer.playing === false) {
                $scope.smarterPod.resyncUpdateFeed();
            }
        }, 3600000);

        $rootScope.$on('ngDialog.closed', function (e, $dialog) {
           $scope.results = [];
        });

        $scope.addOrRemove = function (addOrRemove, index, location, option, option2) {
            if (addOrRemove === 'remove') {
                $scope.smarterPod.removeItemFromPodList(index, $scope.smarterPod.podList[index + 1]);
                $scope.currentPlayingImg = $scope.smarterPod.podPlayer.image.toString();
                if (currentPage.playerScope.podList.length === 0) {
                    currentPage.playerScope.podPlayer.currentTime = 0;
                }
            }
            else {
                currentPage.playerScope.podPlayer.currentTime = 0;
                $timeout(function () {
                    if (option === "saved-item") {
                        if (option2) {
                            $scope.smarterPod.playNow(location, index, 'saved', 'abandon');
                        }
                        $scope.smarterPod.playNow(location, index, 'saved');
                    }
                    else {
                        $scope.smarterPod.$digest();
                        $scope.smarterPod.playNow(location, index)
                    }
                    $scope.currentPlayingImg = $scope.smarterPod.podPlayer.image.toString();
                }, 1000)
            }
        };

        $scope.trackValue = $scope.smarterPod.podPlayer.formatTime + " out of " + $scope.smarterPod.podPlayer.formatDuration;

        $interval(function () {
            $scope.trackProgress = ($scope.smarterPod.podPlayer.currentTime / $scope.smarterPod.podPlayer.duration) * 100 + '%';
        }, 500);

        $interval(function () {
            $scope.trackValue = $scope.smarterPod.podPlayer.formatTime + " out of " + $scope.smarterPod.podPlayer.formatDuration;
        }, 500);

        $scope.addTrack = function (index, location) {
            if ($scope.playlistLength === 0) {
                $scope.smarterPod.playNow(location, index);
                $scope.currentPlayingImg = $scope.smarterPod.podPlayer.image.toString();
            }
            $scope.smarterPod.addTrack(index);
        };

        $scope.saveForLater = function (data) {
            $scope.smarterPod.podPlayer.pause();
            $scope.smarterPod.saveState(data);
        };

        $scope.volumeChange = function (increaseOrDecrease) {
            var currentVolume = $scope.smarterPod.podPlayer.volume;
            if (increaseOrDecrease === 'increase' && currentVolume < 1) {
                $scope.smarterPod.podPlayer.setVolume(currentVolume + 0.25);
            }
            else if (increaseOrDecrease === 'decrease' && currentVolume >= 0.25) {
                $scope.smarterPod.podPlayer.setVolume(currentVolume - 0.25)
            }
        };

        $scope.results;

        $scope.searchForPodcasts = function (podcastSearchName) {

            $scope.smarterPod.searchForPodcasts(podcastSearchName).then(function (data) {
                $scope.results = data;
            })
        };

        $scope.openDialog = function (boxName) {
            var template = boxName + '-template';
            ngDialog.open({template: template, className: 'ngdialog-theme-default', scope: $scope});
        };

        $scope.removeFeed = function (index) {
            $scope.smarterPod.playlistInteraction.userFeed.splice(index, 1);
            $scope.smarterPod.saveFeedlist();
            $scope.$digest();
        };

        $scope.deleteItemFromUpdateFeed = function (index) {
            $scope.smarterPod.playlistItems.splice(index, 1)
        };

        $scope.skipBackward = function () {
            var currentTime = $scope.smarterPod.podPlayer.currentTime;
            currentPage.playerScope.podPlayer.seek(currentTime - $scope.fastForwardLength);
        };

        $scope.fastForward = function () {
            var currentTime = $scope.smarterPod.podPlayer.currentTime;
            currentPage.playerScope.podPlayer.seek(currentTime + $scope.fastForwardLength);
        };

    });