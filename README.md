# smarterPod

smarterPod is a Angular.js based Google Chrome extension for listening to podcasts, in the background of your browser, with feed management capabilities, Chrome notifications on title change, metadata retrieval as well as Google Storage Sync capability for your partially listened episodes and current playlists, regardless of which computer you're on.

## Overview

I've been toying with the idea of smarterPod for a while now, mostly due to the fact that I couldn't passively listen to podcasts on my laptop whilst surfing the internet, without having to keep an extra tab open or fiddle around with Flash-based media clients. 

smarterPod uses the capabilities of Google Storage Sync to keep feeds and episodes in sync across browsers (work vs home, for instance), to ideally provide a simple, seamless experience.

smarterPod consists of a front-end 'remote' Angular app to control the player, display the current state, manipulate the player and feeds, as well as a robust back-end Angular app which parses RSS feeds, manages sync with Google's Chrome Storage, manages the player's events, Chrome notifications, functionality and scope, and manages the playlist for the application.

## Release and Recognition

smarterPod has been released in the Chrome Web Store.

smarterPod was named Extension of the Week on <a href="https://gigaom.com/2015/01/21/chrome-show-two-new-acer-chromebooks-for-education-c740-c920/">GigaOm's Chrome Show</a> for the week of Jan 21st, 2015.

## Installation

Installation instructions below are for installing the extension as an unpacked directory of files.

<ul>
	<li>Access the Chrome Extensions menu in your version of Chrome. This can be found at chrome://extensions.</li>
	<li>Click the 'Developer Mode' checkbox (if this is not done so already).</li>
	<li>Click the 'Load Unpacked Extension...' button.</li>
	<li>Access the local directory where you unpacked smarterPod.</li>
	<li>Point to the 'client' folder inside the directory.</li>
</ul>

##Supported Functionality

Currently, the basics of playlist management, 'save state' functionality, adding/removing feeds, as well as (of course) listening to podcasts in the background is functional. 

## Current Status / Release History

###0.3

Officially deployed to all users as of March 12, 2016. (Yikes.)

Wow, it's been a while. Sorry about the lack of updates, but the next few weeks, they're going to be coming fast and furious.

Added iTunes Podcast Search Functionality (this is a fun one), but kept the ability to add an RSS feed manually.

TODO: Revamp the UI completely -- no more update feed, auto add to playlist, cleaner subscribe module. More streamlined experience. A chicken in every garage and a car in every pot.

###0.2.2

Officially deployed to all users as of April 4, 2015.

Decided to build out the 'remove from update feed' feature and release early.

Also, as an additional feature, added 'force refresh' button to Update Feed, in case users are not seeing updated episodes of their newest podcasts in their Update Feed.

###0.2.1

Officially deployed to all users as of April 3, 2015.

Renamed: Newest episode list --- > SmarterPod Update Feed.

Beta Feature: Older Episode Download. Users can now pull the previous episode in a feed, add it to the Update Feed, and then add episodes to playlist.

TODO: Ability for users to remove episode from update feed.

###0.2.0.2

Officially deployed to all users as of March 31, 2015.

Minor release to fix feed refresh bug and to modify popup styling slightly.

###0.2.0.1

Officially deployed to all users as of March 26, 2015.

Major release to implement visual redesign of the entire application, utilizing Google's material design standards. Redesign utilizes MaterializeCSS library for CSS and JS style improvements.

Between 0.1.1 and 0.2.0.1, implemented minor hot-fixes to fix minor bugs. 

Major bug was fixed that caused multiple saved items to not be saved in different places. This was not reported but was found during the redesign process.

###0.1.1
Officially deployed to all users as of Jan 30, 2015. Users who have installed from Web Store will need to 're-enable' the extension.

In this release, extension has been altered in order to enable cross-origin requests, to remove the YQL-RSS parsing logic, in favor of <b>significantly</b> more efficient RSS feed parsing on the extension level via Angular-XML 2.0's internal JSON conversion functionality. As a result, users who installed will see the added permissions issue.

This is a bit of a backpedal from the 0.1.0.8 release, where smarterPod utilized YQL for feed parsing. After careful consideration, it felt more self sufficient to do all conversion within the engine.

###0.1.0.8
Officially deployed to all users as of Jan 23, 2015. 

In this release: bugfixes, including but not limited to:

<ul>
	<li>Multiple RSS feeds with same URL breaks app.</li>
	<li>Bug tracking using BugSnag</li>
	<li>Revised settings menu text.</li>
</ul>

Moreover, XML Parsing in backend of app was removed earlier than anticipated in favor of conversion to JSON on YQL serverside.

###0.1
This is the alpha version of this extension released as of Nov 12, 2014. There are a few bugs that are evident and immediately obvious, which are listed below.
<ul>
	<li>Sync Issues: Currently, sync is limited to a set size of 4KB per item. So, the list of episodes that can currently sync is a bit finite (around 5 items in playlist). Therefore, a split then join (and the opposite) method should be done in the backend to ensure full compatibility with Google Chrome sync services.</li>
	<li>Player Bugginess: Occasionally, the player will not play upon the first 'PLAY' or 'Play Now' click. </li>
	<li>Playlist Bugginess: If a user is listening to a particular item, and they add a new item to the playlist, the player stops. The severity of this bug has been mitigated somewhat by an immediate 'Save State' function, which saves the position the user is in, but this functionality is not great UX and should be improved expediently.</li>
	<li>Universal Podcast RSS Compatibility: smarterPod has been tested with feeds from major providers such as Libsyn, Feedburner and Soundcloud. While Podcast XML feeds tend to follow a universal schema as dictacted by the standard, some feeds have a tendency to deviate from the schema, resulting in update issues. I've written the parsing function to account for a degree of variance in structure, the user may encounter an RSS feed that cannot be read. The parser will need to be rewritten, using JSON manipulation and better error handling.</li>
	<li>Notification Bugginess: Chrome notification functionality is a bit wonky. It works, but often gives 'too many' notifications, often when a user is clicking 'Play Now' from their playlist.</li>
</ul> 

## Future Features
There are a number of features that are planned to be built out in the next releases. These features include (but aren't limited to!)...

<ul>
	<li>Customizable Fast Forward Length (eg: Fast Forward is 30 seconds, 60 seconds, 90 seconds, etc)</li>
	<li>Multi-episode Feed Updates (eg: 3 episodes from X feed, 4 episodes from Y feed on update)</li>
	<li>Custom RSS Update Time/Interval</li>
	<li>Player Control From Notifications</li>
	<li>Rich Notifications on Feed Updates, including 'Add To Playlist' button</li>
	<li>And the big one: when the Chrome Extension is improved and ready... companion apps for mobile including full sync, built in Google Apps for Mobile.</li>
</ul>

##Support Development
I only ask for one (or all) of these three things to if you wish to help support the development of smarterPod.
<ul>
	<li>Subscribe to the comedy podcast I cohost - <a href="http://www.manchildshow.com">Manchild: A Podcast About Grown Up Stuff.</a> The RSS feed is located at <a href=" http://manchild.libsyn.com/rss">this link</a>, and it is also available on iTunes, Stitcher and Windows Phone. </li>
	<li>Contribute To The Codebase: contributing is VERY welcome. </li>
	<li>Donate to the Electronic Frontier Foundation: As the podcasting community grows, the EFF remains a staunch defender of podcasters' / internet broadcasters' rights.</li>
</ul>

##Credits

There are a few key modules that are used within this project, without which, smarterPod would have been significantly more difficult to build. They are:

<ul>
	<li><a href="https://github.com/mrgamer/angular-media-player">angular-media-player</a>: a huge key to the media functionality within this project.</li>
	<li><a href="https://github.com/johngeorgewright/angular-xml">angular-xml</a>: used for parsing/metadata retrieval.</li>
	<li><a href="http://bugsnag.com">BugSnag</a>: for error tracking.</li>
</ul>


##Author

This work was initially created over the course of several days in November 2014 by <a href="http://www.steveromain.com">Steve Romain</a>.
